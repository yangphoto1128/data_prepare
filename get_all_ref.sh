#!/bin/bash

#準備語料格式-自製
stage=1
dir=wav_file/Aladdin_intents

echo "data prepare for home made corpus"

if [ $stage -le 1 ]
then
	echo "cat all subset ref together"
    pushd $dir
    subsets=`echo */`
    for i in $subsets
    do
	    cat ${i}ref_data_small.csv | tail -n +2 >> ../../ref_all.csv
	
    done
    popd


fi

#!/bin/bash

#準備語料-自製格式

#------------antribute-----------------
stage=1
ref_file=ref_all.csv
subset_name="train_ai_lamp"
#-------------------------------------

echo "data prepare for home made corpus"

if [ $stage -le 1 ]
then
    
	echo "data prepare for kaldi"
    echo "subset:"${subset_name}

	dir=data/${subset_name}
	mkdir $dir

    #cat into temp file
    cat ${ref_file} | tail -n +1 > $dir/ref_temp.txt
    pushd $dir

	#wav.scp
    awk -F',' '{print $4}' ref_temp.txt  > temp_utt_id
    awk -F',' '{print $5}' ref_temp.txt  > temp_wav
    awk '{ printf $0; getline<"temp_wav"; print","$0 }' temp_utt_id | sed 's/,/ /g' > wav.scp

	#text
    awk -F',' '{print $3}' ref_temp.txt > temp_text
    awk '{ printf $0; getline<"temp_text"; print","$0 }' temp_utt_id | sed 's/,/ /g' > text

	#utt2spk
    awk -F',' '{print $1}' ref_temp.txt > temp_spk
    awk '{ printf $0; getline<"temp_spk"; print","$0 }' temp_utt_id | sed 's/,/ /g' > utt2spk
    
    #spk2utt 以下指令需要安裝kaldi
    #cat text | awk '{printf("%s global\n", $1);}' > utt2spk
    #utils/utt2spk_to_spk2utt.pl <data/$x/utt2spk >data/$x/spk2utt

    rm temp*
    rm ref_temp.txt

    popd

    echo "done"

fi

    #awk '{ printf $0; getline<"temp_spk"; print","$0 }' temp_utt_id> spk2utt
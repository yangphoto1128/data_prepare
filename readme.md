# kaldi data prepare

## 基礎資料
- kaldi 必須文件 （須從標記的文本擷取出來）
- wav.scp: 每一個音檔對應的ID/檔案位置
- text: 每一個音檔的ID與其標記的內容
- utt2spk: 每一個音檔ID與對應語者的ID
- spk2utt: 每一個語者的ID與對應音檔的ID
- 使用utils/spk2utt_to_utt2spk.pl 或 utils/utt2spk_to_spk2utt.pl 可以互相轉換

## 語言模型相關
- 語言模型相關資料 - 在語言知識方面 kaldi 需要以下資訊：
- lexicon.txt: 發音辭典
- lexiconnp.txt: 發音辭典+發音機率
- silence_phone.txt 靜音類音素
- nonsilience_phone.txt 語言相關的真實音素
- optional_silence.txt 備用靜音素
- extra_questions.txt 同一行的音素有一樣的重音或音調 與gmm訓練中產生的question	 一起生成決策樹

## scripts
get_all_ref.sh: 把所有子集合ref 複製到一起
prep_selfdata.sh: 由自製語料產生kaldi格式文件
#encoding utf-8
#random pick ref for testing
import csv
import random

def picking(hops,times=2,n=2,data=[]):
    new_data=[]
    start=0
    for x in range(times):
        # pick n sample

        print("pick "+str(n)+" samples from "+str(start)+" to "+str(start+hops))
        for y in range(n):
            i=random.randrange(start,start+hops)
            new_data.append(data[i])

        #更新起點
        start=start+hops

    print("finish")
        
    return new_data

if __name__=="__main__":

    ref_file="ref_all.csv"
    hops=233

    #read data to list
    data=[]
    with open(ref_file) as csv_file:
        rows=csv.reader(csv_file)
        for row in rows:
        # print(row)
            data.append(row)

    #pick up data -> new list
    select=picking(hops=hops,data=data)

    #write new file
    #...
    with open("ref_selected.csv","w") as wf:
        for x in select:
            print(x,file=wf)



